import socket                           #library for socket
from threading import Thread            #multithreading

mt_client = socket.socket()             #connected to socket __it include port and ip
mt_client.connect(("localhost",2002))   #
print("server connected")

def recive_data():                      #function for recive data from server
    while True:                         #loop
        data = mt_client.recv(2000)     #define the byte recive at a time
        print("server:",data.decode())  #decode the data and print


def send_data():                                #fuction for sending data
    while True:
        user_input = input()
        mt_client.sendall(user_input.encode())  #encode the data and sendall data to server
t=Thread(target = recive_data)                  #thread the recive
t.start()                                       #start threading
send_data()                                     #call fuction send_data()
