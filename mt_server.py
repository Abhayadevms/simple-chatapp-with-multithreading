import socket                 #socket library
from threading import Thread  #multithreading

mt_server = socket.socket()      #CONNECT THE SOCKET
port_number = 2002               #define port
mt_server.bind(("",2002))        #bind the port
mt_server.listen()               #listen the server
conn,addr = mt_server.accept()   #accept the server store in two variable conn and addr

def recive_data():                #same as client below all
     while True:
         data = conn.recv(2000)
         print("client",data.decode())

def send_data():
    while True:
        user_input =input()
        conn.sendall(user_input.encode())

t =Thread(target = recive_data)
t.start()
send_data()